<?xml version="1.0" encoding="utf-8"?>

<!-- The previous line is the code to declare this text is not only a text but also a code -->
<!-- The "minus-than + exclamation point + dash + dash" at the begin of these lines is the code to make the interpreter skip that part -->
<!-- The following line is the code to declare this text-also-code as svg (scalable vector graphics) code -->

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">

<!-- Certificate of authenticity/Code for a free detail of the work of art -->
<!-- PACE, 2014 - 2028 edition of 15 for BeLa Editions in Brussels and 5 artist proofs -->
<!-- This is the certificate for artwork -->
<!-- PACE, 2014 -->
<!-- Simona Denicolai & Ivo Provoost, 2009 -->
<!-- The original work of art is accompanied by this certificate of authenticity and this code for a free detail of the work of art that can be reproduced at any size and at any number of exemplars according to the Free Art License 1.3 (Read more about the License in this document) -->
<!-- By work of art we mean the print sticked on aluminium you purchased at Bela Editions in Brussels -->
<!-- The size of the work of art is 80 x 100 cm or 2834.646 x 2267.717 pixels, unless it is an artist proof version printed at 101% of that size -->
<!-- By free detail we mean the image that you will discover by opening this document -->
<!-- This present text by Simona Denicolai & Ivo Provoost and Pierre Huyghebaert -->
<!-- Vectorisation and code by Pierre Huyghebaert -->
<!-- Published by Michele Lachowsky and Joel Benzakin, very special thanks to them -->

<!-- The previous twelve (12) lines form the Certificate of authenticity, only valid if accompagnied by the original work of art  -->

<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->

<!-- Technical instructions  -->

<!-- To read this code, open the svg file in a capable text editor (like the free http://www.openoffice.org, or in Microsoft Word but only if you know it well and are able to force it to open it as text, or you'll get an error) -->

<!-- But, good news, you are reading this code, so copy/paste it in any text editor, then save it as text, and add the .svg extension -->

<!-- To visualize the free detail of the work of art, open the svg file in a modern internet browser (like the free http://www.firefox.com), or drag and drop the svg file in an open window of this browser. You can also print it, but as the original size is huge, you'll need to instruct the browser to reduce it before printing it on a A4 for example -->

<!-- To visualize, print or modify the free detail of the work of art, open the svg file in a vector graphic editor (like the free http://www.inkscape.org) -->

<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!-- The previous near empty lines are only there to attract your attention on the technical instructions (if needed) -->

<!-- The following lines are the code to initiate the drawing of the free detail of the work of art -->

<svg version="1.0"
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
x="0px" y="0px"
width="2834.646px" height="2267.717px"
viewBox="0 0 2834.646 2267.717"
enable-background="new 0 0 2834.646 2267.717"
xml:space="preserve">
<g id="pace">

<!-- The following lines form the code to draw the P of the free detail of the work of art -->

<path fill="#CDDDDA" d="M512.531, 734.288c-5.266, 39.475-26.32, 39.475-50.003, 36.845c-23.684-2.63-57.896-13.162-94.74-39.475c5.266-47.37, 9.698-76.266, 7.896-110.528c42.107, 23.687, 68.424, 18.421, 97.37, 36.841C502.003, 676.393, 517.794, 694.813, 512.531, 734.288z M185.8, 794.813c28.946, 92.11, 55.266, 136.848, 55.266, 255.274c18.421, 5.266, 42.104, 7.889, 63.158, 23.68c21.05, 15.792, 7.896, 26.32, 44.737, 36.846c-15.788-105.262-13.158-184.218, 2.633-242.11c26.313, 10.528, 73.687, 23.687, 115.791, 36.842c42.107, 13.161, 118.423, 26.316, 160.531, 13.161c12.549-3.921, 25.239-13.612, 32.902-22.074c1.337, 0.212, 2.649, 0.399, 4.645, 0.667c19.808-21.295, 32.08-54.192, 33.913-70.703c2.63-23.687-10.525-84.211-15.791-110.528c-5.263-26.316-5.263-71.05-39.471-107.895c-34.212-36.845-73.687-71.057-144.743-92.107c-71.053-21.053-210.53-44.74-255.268-65.791c-44.74-21.054-42.107-28.953-73.683-50.006c2.63, 65.794-18.424, 94.74-15.791, 160.531c2.062, 51.603, 7.979, 119.057, 23.44, 186.15c-3.948-3.665-3.812-3.601-7.203-6.533C175.116, 758.664, 179.558, 774.951, 185.8, 794.813z"/>

<!-- The following lines form the code to draw the A of the free detail of the work of art -->

<path fill="#CDDDDA" d="M842.067, 927.465c1.797, 0.352, 3.605, 0.711, 5.426, 1.079c0.294-1.446, 9.762-50.742, 12.413-62.674c5.263-23.684, 63.155-100.003, 73.684-142.107c10.528-42.104, 21.057-73.687, 23.687-97.373c57.895, 10.532, 176.319, 13.162, 226.322, 18.427c34.208, 71.05, 60.525, 147.366, 76.316, 173.683c15.791, 26.319, 15.788, 52.636, 23.684, 73.689c6.453, 17.205, 25.457, 51.793, 33.757, 69.853c-2.307, 0.024-3.811, 0.062-5.668, 0.001c13.5, 31.122, 32.033, 69.623, 45.195, 98.569c13.158, 28.946, 39.471, 68.424, 47.367, 97.373s34.211, 78.945, 47.369, 107.895c-55.262, 5.264-144.739, 5.27-221.055, 2.633c-9.399-17.229-26.253-62.422-41.135-105.62c-12.655-0.355-27.587-2.28-48.342-2.28c-34.209, 0-84.212-2.631-121.057-5.26c-15.401-1.102-28.937-0.371-40.849-0.293c-11.142, 29.758-24.618, 63.699-30.204, 89.768c-65.791-7.896-136.844-18.415-228.952-31.582c13.158-23.688, 26.316-44.738, 31.579-57.893c5.263-13.158, 65.791-134.218, 76.319-168.427C833.143, 969.952, 837.487, 950.336, 842.067, 927.465z M1028.159, 946.415c-5.005, 16.688-9.83, 40.317-10.762, 58.932c0, 0.004, 0, 0.01, 0, 0.013c40.251, 5.379, 85.901, 8.87, 119.975, 6.615c-9.242-22.847-16.468-39.781-24.199-58.496c1.887-0.035, 3.762-0.075, 5.621-0.121c-0.792-1.91-6.919-16.791-8.882-21.697c-15.791-39.472-18.421-81.579-28.949-110.525c-18.708, 45.921-36.71, 87.925-47.134, 125.279C1032.086, 946.366, 1030.404, 946.352, 1028.159, 946.415z"/>

<!-- The following lines form the code to draw the C of the free detail of the work of art -->

<path fill="#CDDDDA" d="M1464.321, 959.003c1.882-0.087, 3.759-0.17, 5.63-0.248c1.37-64.653, 9.842-127.106, 27.948-173.203c18.112-46.103, 70.796-100.427, 124.296-127.586c53.508-27.166, 80.68-42.806, 172.041-59.27c87.386-15.747, 138.291-18.106, 202.503, 19.754c64.202, 37.867, 73.253, 63.381, 79.843, 80.669c6.58, 17.286, 9.053, 39.513, 5.762, 71.617c-40.339, 7.405-54.33, 13.171-84.787, 19.754c-30.459, 6.587-47.744, 12.344-82.314, 18.109c-5.763-20.582-24.693-40.336-41.979-49.389c-17.291-9.057-45.463-19.028-83.139-10.702c-39.1, 8.644-72.749, 31.796-93.025, 65.848c-18.806, 31.615-27.26, 82.208-20.853, 129.31c-1.969, 0.267-3.759, 0.659-5.545, 0.929c14.413, 108.216, 55.151, 150.751, 117.864, 143.88c68.316-7.486, 121.006-61.742, 127.586-135.821c61.743-9.06, 116.886-22.233, 180.271-40.333c-2.46, 125.941-30.016, 164.799-107.833, 251.883c-34.57, 38.686-121.83, 76.553-220.602, 82.314c-98.773, 5.757-164.627-21.406-226.366-83.139c-17.288-17.285-27.987-37.867-41.157-62.561C1488.026, 1077.505, 1465.286, 1042.491, 1464.321, 959.003z"/>

<!-- The following lines form the code to draw the E of the free detail of the work of art -->

<path fill="#CDDDDA" d="M2606.211, 853.108c5.37, 34.897, 8.054, 46.977, 16.106, 79.188c-28.185, 9.393-57.71, 16.105-84.555, 26.838c-26.842, 10.739-38.921, 9.399-67.105, 20.132c-13.835, 5.272-18.514, 8.026-24.447, 13.016c-8.273, 6.957-12.067, 22.397-24.812, 37.237c-34.766, 40.482-77.216, 56.737-95.549, 66.513c-20.133, 10.739-41.607, 24.162-96.635, 33.558c-4.025-24.159-12.078-41.61-18.788-87.241c-6.706-45.631-22.818-104.687-25.502-132.871c-2.089-21.889-2.006-31.896-3.995-57.634c1.879-0.651, 3.738-1.303, 5.562-1.958c-0.138-1.915-1.708-21.334-2.607-30.328c-4.025-40.264-6.713-83.214-8.053-122.139c-1.343-38.917, 0-69.789, 0-93.947c0-24.158-5.369-55.03-6.713-76.501c38.918-9.34, 57.196-14.247, 92.607-22.818c61.698-14.935, 108.713-29.522, 150.32-42.944c41.607-13.426, 84.555-16.105, 134.215-61.742c49.656-45.631, 92.607-83.208, 130.186-96.634c9.396, 33.558, 22.818, 80.528, 18.791, 132.871c-59.056, 16.106-73.818, 24.159-112.738, 38.924c-38.925, 14.766-65.767, 37.578-102.004, 60.396c-36.238, 22.818-52.344, 37.578-65.766, 45.63c-13.42, 8.053-25.498, 13.425-41.604, 22.818c1.34, 21.472, 1.346, 44.293, 0, 67.109c49.656-33.558, 73.814-40.264, 110.053-59.056c36.237-18.792, 81.871-32.211, 106.029-38.924c20.879-5.797, 40.677-8.65, 66.006-20.33c-16.52, 35.145-31.322, 67.332-43.333, 94.358c-9.108, 20.496-19.108, 42.466-29.838, 65.526c-7.444, 5.635-18.304, 10.594-29.373, 14.795c-8.053, 3.056-29.528, 8.053-81.871, 22.812c-52.343, 14.766-57.713, 21.479-84.555, 32.211c4.026, 38.924, 16.105, 68.449, 24.159, 115.426c21.475-10.739, 30.867-9.393, 46.973-14.766c13.655-4.55, 37.036-20.34, 60.88-38.238c10.971-8.235, 11.676-28.243, 23.872-39.254c8.307-7.499, 12.666-10.209, 22.479-15.115c8.053-4.026, 22.815-8.05, 32.211-10.733C2599.502, 791.372, 2600.846, 818.21, 2606.211, 853.108z"/>

<!-- The following lines form the code to draw the folding line of the free detail of the work of art -->

<path fill="#656A79" d="M2611.882, 646.627c-73.645, 165.7-205.1, 427.14-315.631, 591.818c-51.689, 77.004-76.283, 117.129-110.247, 171.668c15.479-21.134, 40.22-57.466, 56.676-82.581s27.231-42.621, 56.915-86.844c110.778-165.061, 242.23-426.536, 315.967-592.426c36.817-82.843, 99.962-214.396, 158.388-330.845c21.371-42.595, 42.107-83.167, 60.696-118.6v-8.653C2765.057, 322.301, 2663.1, 531.384, 2611.882, 646.627z"/>

<!-- The following lines form the code to finalize the free detail of the work of art -->

</g>
</svg>

<!-- The following lines are the licence of release of the free detail of the work of art -->

<!-- Free Art License 1.3 (FAL 1.3) -->

<!-- Preamble -->
<!-- The Free Art License grants the right to freely copy, distribute, and transform creative works without infringing the author's rights. The Free Art License recognizes and protects these rights. Their implementation has been reformulated in order to allow everyone to use creations of the human mind in a creative manner, regardless of their types and ways of expression. While the public's access to creations of the human mind usually is restricted by the implementation of copyright law, it is favoured by the Free Art License. This license intends to allow the use of a work's resources; to establish new conditions for creating in order to increase creation opportunities. The Free Art License grants the right to use a work, and acknowledges the right holder's and the user's rights and responsibility. The invention and development of digital technologies, Internet and Free Software have changed creation methods: creations of the human mind can obviously be distributed, exchanged, and transformed. They allow to produce common works to which everyone can contribute to the benefit of all. The main rationale for this Free Art License is to promote and protect these creations of the human mind according to the principles of copyleft: freedom to use, copy, distribute, transform, and prohibition of exclusive appropriation. -->

<!-- Definitions -->
<!-- ''work'' either means the initial work, the subsequent works or the common work as defined hereafter: -->
<!-- ''common work'' means a work composed of the initial work and all subsequent contributions to it ''originals and copies''. The initial author is the one who, by choosing this license, defines the conditions under which contributions are made. -->
<!-- ''Initial work'' means the work created by the initiator of the common work (as defined above), the copies of which can be modified by whoever wants to -->
<!-- ''Subsequent works'' means the contributions made by authors who participate in the evolution of the common work by exercising the rights to reproduce, distribute, and modify that are granted by the license. -->
<!-- ''Originals'' (sources or resources of the work) means all copies of either the initial work or any subsequent work mentioning a date and used by their author(s) as references for any subsequent updates, interpretations, copies or reproductions. -->
<!-- ''Copy'' means any reproduction of an original as defined by this license. -->

<!-- 1. Object -->
<!-- The aim of this license is to define the conditions under which one can use this work freely. -->

<!-- 2. Scope -->
<!-- This work is subject to copyright law. Through this license its author specifies the extent to which you can copy, distribute, and modify it. -->

<!-- 2.1 Freedom to copy (or to make reproductions) -->
<!-- You have the right to copy this work for yourself, your friends or any other person, whatever the technique used. -->

<!-- 2.2 Freedom to distribute, to perform in public -->
<!-- You have the right to distribute copies of this work; whether modified or not, whatever the medium and the place, with or without any charge, provided that you: -->
<!-- attach this license without any modification to the copies of this work or indicate precisely where the license can be found, -->
<!-- specify to the recipient the names of the author(s) of the originals, including yours if you have modified the work, -->
<!-- specify to the recipient where to access the originals (either initial or subsequent). -->
<!-- The authors of the originals may, if they wish to, give you the right to distribute the originals under the same conditions as the copies. -->

<!-- 2.3 Freedom to modify -->
<!-- You have the right to modify copies of the originals (whether initial or subsequent) provided you comply with the following conditions: -->
<!-- all conditions in article 2.2 above, if you distribute modified copies; -->
<!-- indicate that the work has been modified and, if it is possible, what kind of modifications have been made; -->
<!-- distribute the subsequent work under the same license or any compatible license. -->
<!-- The author(s) of the original work may give you the right to modify it under the same conditions as the copies. -->

<!-- 3. Related rights -->
<!-- Activities giving rise to author's rights and related rights shall not challenge the rights granted by this license. -->
<!-- For example, this is the reason why performances must be subject to the same license or a compatible license. Similarly, integrating the work in a database, a compilation or an anthology shall not prevent anyone from using the work under the same conditions as those defined in this license. -->

<!-- 4. Incorporation of the work -->
<!-- Incorporating this work into a larger work that is not subject to the Free Art License shall not challenge the rights granted by this license. -->
<!-- If the work can no longer be accessed apart from the larger work in which it is incorporated, then incorporation shall only be allowed under the condition that the larger work is subject either to the Free Art License or a compatible license. -->

<!-- 5. Compatibility -->
<!-- A license is compatible with the Free Art License provided: -->
<!-- it gives the right to copy, distribute, and modify copies of the work including for commercial purposes and without any other restrictions than those required by the respect of the other compatibility criteria; -->
<!-- it ensures proper attribution of the work to its authors and access to previous versions of the work when possible; -->
<!-- it recognizes the Free Art License as compatible (reciprocity); -->
<!-- it requires that changes made to the work be subject to the same license or to a license which also meets these compatibility criteria. -->

<!-- 6. Your intellectual rights -->
<!-- This license does not aim at denying your author's rights in your contribution or any related right. By choosing to contribute to the development of this common work, you only agree to grant others the same rights with regard to your contribution as those you were granted by this license. Conferring these rights does not mean you have to give up your intellectual rights. -->

<!-- 7. Your responsibilities -->
<!-- The freedom to use the work as defined by the Free Art License (right to copy, distribute, modify) implies that everyone is responsible for their own actions. -->

<!-- 8. Duration of the license -->
<!-- This license takes effect as of your acceptance of its terms. The act of copying, distributing, or modifying the work constitutes a tacit agreement. This license will remain in effect for as long as the copyright which is attached to the work. If you do not respect the terms of this license, you automatically lose the rights that it confers. -->
<!-- If the legal status or legislation to which you are subject makes it impossible for you to respect the terms of this license, you may not make use of the rights which it confers. -->

<!-- 9. Various versions of the license -->
<!-- This license may undergo periodic modifications to incorporate improvements by its authors (instigators of the ''Copyleft Attitude'' movement) by way of new, numbered versions. -->
<!-- You will always have the choice of accepting the terms contained in the version under which the copy of the work was distributed to you, or alternatively, to use the provisions of one of the subsequent versions. -->

<!-- 10. Sub-licensing -->
<!-- Sub-licenses are not authorized by this license. Any person wishing to make use of the rights that it confers will be directly bound to the authors of the common work. -->

<!-- 11. Legal framework -->
<!-- This license is written with respect to both French law and the Berne Convention for the Protection of Literary and Artistic Works. -->

<!-- The following lines are an explanation on the release of the free detail of the work of art -->

<!-- User guide -->

<!-- - How to use the Free Art License? -->
<!-- To benefit from the Free Art License, you only need to mention the following elements on your work: -->
<!-- [Name of the author, title, date of the work. When applicable, names of authors of the common work and, if possible, where to find the originals]. -->
<!-- Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/ -->

<!-- - Why to use the Free Art License? -->
<!-- 1. To give the greatest number of people access to your work. -->
<!-- 2. To allow it to be distributed freely. -->
<!-- 3. To allow it to evolve by allowing its copy, distribution, and transformation by others. -->
<!-- 4. So that you benefit from the resources of a work when it is under the Free Art License: to be able to copy, distribute or transform it freely. -->
<!-- 5. But also, because the Free Art License offers a legal framework to disallow any misappropriation. It is forbidden to take hold of your work and bypass the creative process for one's exclusive possession. -->

<!-- - When to use the Free Art License? -->
<!-- Any time you want to benefit and make others benefit from the right to copy, distribute and transform creative works without any exclusive appropriation, you should use the Free Art License. You can for example use it for scientific, artistic or educational projects. -->

<!-- - What kinds of works can be subject to the Free Art License? -->
<!-- The Free Art License can be applied to digital as well as physical works. -->
<!-- You can choose to apply the Free Art License on any text, picture, sound, gesture, or whatever sort of stuff on which you have sufficient author's rights. -->

<!-- - Historical background of this license: -->
<!-- It is the result of observing, using and creating digital technologies, free software, the Internet and art. It arose from the ''Copyleft Attitude'' meetings which took place in Paris in 2000. For the first time, these meetings brought together members of the Free Software community, artists, and members of the art world. The goal was to adapt the principles of Copyleft and free software to all sorts of creations. http://www.artlibre.org -->

<!-- The following lines are of the licence of the free detail of the work of art -->

<!-- Copyleft Attitude, 2007. You can make reproductions and distribute this license verbatim (without any changes). -->